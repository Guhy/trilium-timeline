# trilium-timeline

Version 1.5

Interactive timeline in html+js widget for [Trilium Notes](https://github.com/zadam/trilium), using [vis.js](https://visjs.org/) timeline.

## What is this?

Fetches notes with specific labels to be displayed as events in various ways (box, single point, range, background) grouped in multiple categories and with varying colors, uses root note's promoted attributes to easily set the timeline values (present marker, start/end values for the initial loaded view), defined which labels should be fetched and defined the types/groups in which to sort events.

Used for my own personal purposes (prepping and tracking a Pathfinder 2e tabletop campaign) and might require some tweaking for other purposes, but the customizable attributes should be enough.

![Preview](https://user-images.githubusercontent.com/12102823/138761229-300d65fa-9b6d-4693-a5e0-344bf8718fa8.png)

## Install

Download this repo and extract it.
Inside of Trilium, right click the folder you want to import into and click "Import into Note" at the bottom.
Select the "Timeline Widget Import.zip" archive.
Import without changing any of the the default options (although if you get issues maybe try unchecking stuff like Safe Import which i'm assuming can sometimes decide to not import things as they are, although I've never had problems).
The Timeline Widget is then imported but nothing is displayed yet, as indicated by the message "Render HTML doesn't have required relation to function properly", because the renderNote attribute has been automatically disabled on import. To fix this, go to the Owned attributes tab at the top and look for "~disabled:renderNote=render.html", normally the last attribute in the list. Click on it and remove the "disabled:" part so as to have "renderNote" as the attribute name. Save and now the widget should display.

## Usage

The setup will require a bit of effort in defining the required attributes, but the widget contains a working example that can be more easily tweaked.

The events (notes fetched by the widget with the proper labels) showing up on the timeline can be hovered and clicked.

#### Understanding the attributes

The widget reads the values of certain attributes ("event_label_start", "event_label_end", "person_label_start", "person_label_end" and "event_label_type") are used to define what labels (or "tags") you want to use for your own setup. The values you input into these attributes will be read by the widget as the labels it should look for.

All date values are in the format "YYYY-MM-DD:hh:mm:ss" but you can enter simply "YYYY", or "YYYY-MM-DD:hh" and not the entire date.

#### Date attributes

The labels defined by the attributes "event_label_start", "event_label_end", "person_label_start" and "person_label_end" are date attributes containing a date value in the text format described previously.

- By default, "event_label_start" is set as "date_start", which means all notes in trilium with a label attribute named "date_start" will be fetched, and the date value in the attribute will be displayed on the timeline.
- By default, "event_label_end" is set as "date_end", which means any note that has already been fetched through "date_start" and also contains the label attribute "date_end" with a date value will be displayed as a range (with valid start and end date values) on the timeline. A note that only contains the defined "event_label_end" label and not the "event_label_start" label will not be fetched (an event note requires a starting date to be seen as valid).

A special case is the "person" type notes. By default, "person_label_start" is set as "date_birth", and "person_label_end" is set as "date_death". Any note which contains *either* of those labels will be fetched (UNLIKE the normal event notes which require a starting date), any starting dates will appear as a "Note Title (Birth)" date on the timeline, and any ending dates will appear as a "Note Title (Death)" date on the timeline. If you need this special case but want to change the title of the generated dates (for something other then Birth and Death), you will have to change the values in *script.js* lines 45 and 56.

#### Group attributes

The label defined by the attribute "event_label_type" is a tagging attribute containing a string/text used to mark your event/person notes and sort them into corresponding groups.

- By default, "event_label_type" is set as "date_type", which means all notes that contain the label attribute "date_type" will be sorted into the group corresponding to the value (if they already contain the label defined by "event_label_start" or by one of the two person labels so they can be detected by the widget).

For example, if you have defined a group with the id "birthdays", and the attribute "event_label_type" is defined as "date_group", then any valid note containing the label "date_group" and with the value "birthdays" will be sorted into the corresponding group on the timeline. If a note has no group, or if the group name isn't valid/does not have a corresponding group defined, it will simply be put into an empty default group.

The groups are the hardest to setup. Declaring your groups is done through the attribute "event_type_list" in a text value. The widget reads this text value to generate the groups and their settings.

The "event_type_list" value follows this format, in () is the type of data it needs to be :
> id(string), order(int), title(string), color(string), visible(bool), display(string), forceGroup(int)

Each group is defined with these values, each value separated by a **, (comma)** and each group separated by a **; (semicolon)**.

- The "id" is a string (such as "birthdays", "first" or "historical"), it is also the value that needs to match the values of the "event_label_type" labels in order to sort events into groups.
- The "order" is an integer value that defines the order in which the groups appear on the timeline, going from top (0) to bottom (99+).
- The "title" is the text that will appear as the title of the group on the timeline and on the UI button.
- The "color" needs to be a valid CSS color value ([CSS color values](https://developer.mozilla.org/en-US/docs/Web/CSS/color_value)) and will determine the color of all boxes in that group on the timeline.
- The "visible" determines if the group is visible by default or not. If the group is not visible by default (and is not forcedInto another group), it can be displayed by clicking the corresponding UI button.
- The "display" determines what kind of date box the events in that group will be displayed as. It has 3 valid values, "box" (the default), "point" (minimal point instead of a box) and "background" (appears in the background of the group, useful for eras/epochs or large stretches of time).
- The "forcedInto" is a special option which allows for notes to be defined into their own group (with their own id, color, display type, etc..) but then merged into another group on timeline display. For example, if you have a group "special days" appearing in blue and another group "birthdays" appearing in red, but you want birthdays to still appear in the wider "special days" group to avoid splitting your timeline into too many groups, you can set the "birthdays" group to have "special days" as the forcedInto value.

By default, "event_type_list" is set as :
> first,1,First,white;second,2,Second,royalblue,false;third,3,Third,orange;forcedInto,4,Forced Into,blueviolet,false,null,third;background,8,Background,null,false,background,second

This reads as : 
- Group of id "first", order 1, title "First", color white ;
- Group of id "second", order 2, title "Second", color royalblue, not visible by default ; 
- Group of id "third", order 3, title "Third", color orange ;
- Group of id "forcedInto", order 4, title "Forced Into", color blueviolet, not visible by default, default display type, events are inserted into the other group of id "third" ;
- Group of id "background", order 8, title "Background", no color, not visible by default, background display type, events are inserted into the other group of id "second" ;

This can take a bit to figure out and setup, but you can also keep it very simple with a few basic groups. The widget contains a working example with groups and notes setup, as well as explanations in the notes.

### Timeline attributes

The widget has 3 label promoted attributes (of date values in the text format described previously), "timeline_start", "timeline_end" and "timeline_present".

- "timeline_start" and "timeline_end" are the range of dates where the view *starts*, to be able to determine where (and how "zoomed-in") you want the timeline to be when loading the widget. You may still scroll and drag to the rest of the full timeline, this only affects the visible part of the timeline in the view when it first loads.
- "timeline_present" optionally shows a red vertical line indicating where the "present" is if you need a timeline where the present isn't actually the present (in my case, to represent the present within the setting of a tabletop campaign, which I change session to session). Be aware this line doesn't stay still, so if you stay on the widget for some time you will notice it moves ahead in real time, but of course resets every time you load/render the widget.
